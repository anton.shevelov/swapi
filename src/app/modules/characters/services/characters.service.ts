import {Injectable} from '@angular/core';
import {ICharactersListResponse} from '../interfaces/characters-list-response';
import {HttpService} from '../../shared/services/http.service';
import {EndpointsService} from '@modules/shared/services/endpoints.service';
import {combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ICharacters} from '../interfaces/characters';

@Injectable({
  providedIn: 'root',
})
export class CharactersService {
  constructor(private http: HttpService, private endpointsService: EndpointsService) {}

  getPeoplesList(
    url: string = this.endpointsService.peoples
  ): Observable<ICharactersListResponse<ICharacters>> {
    return this.http.get<ICharactersListResponse<ICharacters>>(url);
  }

  resolveLinksArray(links: string[] = [], paramName: string): Observable<string[]> {
    return combineLatest([...links.map((link) => this.http.get(link))]).pipe(
      map((responses) => responses.map((item) => item[paramName]))
    );
  }

  getParamsByLink<T>(link: string): Observable<ICharactersListResponse<T>> {
    return this.http.get<ICharactersListResponse<T>>(link);
  }
}
