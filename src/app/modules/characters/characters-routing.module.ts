import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CharactersRoutes} from './characters.routes';
import {CharactersListComponent} from './pages/characters-list/characters-list.component';
import {CharacterInfoComponent} from './pages/character-info/character-info.component';
import {AccessGuard} from './guards/access.guard';
import {CharacterListResolver} from './resolvers/character-list.resolver';

const routes: Routes = [
  {
    path: '',
    redirectTo: CharactersRoutes.CHARACTERS_LIST,
    pathMatch: 'full',
  },
  {
    path: CharactersRoutes.CHARACTERS_LIST,
    component: CharactersListComponent,
    canActivate: [AccessGuard],
    resolve: {characters: CharacterListResolver},
  },
  {
    path: `${CharactersRoutes.CHARACTER_INFO}/:id`,
    component: CharacterInfoComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CaratersRoutingModule {}
