import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CaratersRoutingModule} from './characters-routing.module';
import {CharactersListComponent} from './pages/characters-list/characters-list.component';
import {CharacterInfoComponent} from './pages/character-info/character-info.component';
import {InfoItemComponent} from './pages/character-info/info-item/info-item.component';
import {ParamsFilterComponent} from './components/params-filter/params-filter.component';
import {PaginatorComponent} from './components/paginator/paginator.component';
import {SharedModule} from '@modules/shared/shared.module';

@NgModule({
  declarations: [
    CharactersListComponent,
    CharacterInfoComponent,
    InfoItemComponent,
    ParamsFilterComponent,
    PaginatorComponent,
  ],
  imports: [CommonModule, CaratersRoutingModule, SharedModule],
})
export class CharactersModule {}
