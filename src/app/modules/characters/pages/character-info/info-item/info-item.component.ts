import {Component, Input, OnInit} from '@angular/core';
import {CharactersService} from '@modules/characters/services/characters.service';

@Component({
  selector: 'sw-info-item',
  templateUrl: './info-item.component.html',
  styleUrls: ['./info-item.component.scss'],
})
export class InfoItemComponent implements OnInit {
  @Input() links: string[];
  @Input() paramName: string;
  public params: string[];
  constructor(public charactersService: CharactersService) {}

  ngOnInit(): void {
    this.charactersService.resolveLinksArray(this.links, this.paramName).subscribe((params) => {
      this.params = params;
    });
  }
}
