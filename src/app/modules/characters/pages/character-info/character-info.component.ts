import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CharactersRoutes} from '@modules/characters/characters.routes';
import {CharactersStore} from '@modules/characters/store/characters.store';
import {ICharacters} from '../../interfaces/characters';
import {CharactersService} from '../../services/characters.service';
import {combineLatest} from 'rxjs';

@Component({
  selector: 'sw-character-info',
  templateUrl: './character-info.component.html',
  styleUrls: ['./character-info.component.scss'],
})
export class CharacterInfoComponent implements OnInit {
  public selectedCharacter: ICharacters;
  public charactersMovies: string[];
  public charactersSpaceships: string[];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public charactersStore: CharactersStore,
    public charactersService: CharactersService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.selectedCharacter = this.charactersStore.getCharacterByIndex(params.id);
      if (!this.selectedCharacter) {
        this.router.navigate([
          `${CharactersRoutes.route(CharactersRoutes.CHARACTERS_ROOT)}${CharactersRoutes.route(
            CharactersRoutes.CHARACTERS_LIST
          )}`,
        ]);
        return;
      }
      combineLatest([
        this.charactersService.resolveLinksArray(this.selectedCharacter.films, 'title'),
        this.charactersService.resolveLinksArray(this.selectedCharacter.starships, 'name'),
      ]).subscribe(([films, starships]) => {
        this.charactersMovies = films;
        this.charactersSpaceships = starships;
      });
    });
  }
  openCharactersPage(): void{
    this.router.navigate([
      `${CharactersRoutes.route(CharactersRoutes.CHARACTERS_ROOT)}${CharactersRoutes.route(
        CharactersRoutes.CHARACTERS_LIST
      )}`
    ]);
  }
}
