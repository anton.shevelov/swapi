import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {Observable, of} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ICharactersListResponse} from '../interfaces/characters-list-response';
import {CharactersStore} from '../store/characters.store';
import {ICharacters} from '../interfaces/characters';

@Injectable({
  providedIn: 'root',
})
export class CharacterListResolver implements Resolve<ICharactersListResponse<ICharacters>> {
  constructor(private characterStore: CharactersStore) {}
  resolve(): Observable<ICharactersListResponse<ICharacters>> {
    return this.characterStore.characters.value.length
      ? of(this.characterStore.charactersState.value)
      : this.characterStore.loadCharacterPage().pipe(tap(() => true));
  }
}
