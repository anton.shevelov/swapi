import {Injectable} from '@angular/core';
import {TSubject} from '@modules/shared/classes/tsubject';
import {tap} from 'rxjs/operators';
import {ICharacters} from '../interfaces/characters';
import {ICharactersListResponse} from '../interfaces/characters-list-response';
import {CharactersService} from '../services/characters.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CharactersStore {
  public charactersState: TSubject<ICharactersListResponse<ICharacters>> = new TSubject<
    ICharactersListResponse<ICharacters>
  >(null);
  public characters: TSubject<ICharacters[]> = new TSubject<ICharacters[]>([]);

  constructor(
    private charactersService: CharactersService
  ) {}

  loadCharacterPage(pageUrl?): Observable<ICharactersListResponse<ICharacters>> {
    return this.charactersService.getPeoplesList(pageUrl).pipe(
      tap((characters) => {
        this.charactersState.value = characters;
        this.characters.value = characters.results;
      })
    );
  }
  getCharacterByIndex(index: number): ICharacters {
    return this.characters.value[index];
  }
}
