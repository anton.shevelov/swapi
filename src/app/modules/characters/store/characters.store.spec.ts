import {TestBed} from '@angular/core/testing';

import {CharactersStore} from './characters.store';
import {SharedModule} from '../../shared/shared.module';

describe('CharactersStore', () => {
  let service: CharactersStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
      imports: [SharedModule],
    });
    service = TestBed.inject(CharactersStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
