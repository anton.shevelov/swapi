export interface ICharactersListResponse<T> {
  count: number;
  next: string;
  previous: string;
  results: T[];
}
