import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PaginatorComponent} from './paginator.component';

describe('PaginatorComponent', () => {
  let component: PaginatorComponent;
  let fixture: ComponentFixture<PaginatorComponent>;
  let fixtureEl;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaginatorComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    fixtureEl = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('buttons should be disabled by default', () => {
    fixture.detectChanges();
    expect(fixtureEl.nativeElement.querySelector('#prevBtn').disabled).toBeTruthy();
    expect(fixtureEl.nativeElement.querySelector('#nextBtn').disabled).toBeTruthy();
  });

  it('buttons should be enabled', () => {
    component.prevDisabled = false;
    component.nextDisabled = false;
    fixture.detectChanges();
    expect(fixtureEl.nativeElement.querySelector('#prevBtn').disabled).toBeFalsy();
    expect(fixtureEl.nativeElement.querySelector('#nextBtn').disabled).toBeFalsy();
  });
});
