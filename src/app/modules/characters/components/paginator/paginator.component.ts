import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'sw-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent {
  @Input() prevDisabled = true;
  @Input() nextDisabled = true;
  @Output() prev: EventEmitter<void> = new EventEmitter();
  @Output() next: EventEmitter<void> = new EventEmitter();
  constructor() {}

  onPrevClick(): void {
    this.prev.emit();
  }
  onNextClick(): void {
    this.next.emit();
  }
}
