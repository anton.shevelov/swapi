export class InAppRoutes {
  public static route(routeKey: string): string {
    return `/${routeKey}`;
  }
}
