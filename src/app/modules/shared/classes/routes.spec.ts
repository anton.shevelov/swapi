import {InAppRoutes} from './in-app.routes';

describe('InAppRoutes:', () => {
  it('should return formated route', () => {
    expect(InAppRoutes.route('route')).toEqual('/route');
  });
});
