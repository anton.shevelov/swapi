import {BehaviorSubject} from 'rxjs';

export class TSubject<T> {
  private readonly $value: BehaviorSubject<T> = new BehaviorSubject(null);
  constructor(value: T) {
    this.value = value;
  }
  get value(): T {
    return this.$value.getValue();
  }

  set value(value: T) {
    this.$value.next(value);
  }

  public asyncValue(): BehaviorSubject<T> {
    return this.$value;
  }
}
