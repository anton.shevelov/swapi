import {Pipe, PipeTransform} from '@angular/core';
import {Params} from '@angular/router';

@Pipe({
  name: 'filter',
  pure: false,
})
export class FilterPipe implements PipeTransform {
  transform(value: any[], args: Params): unknown {
    const paramsName = Object.keys(args);

    if (!Object.keys(args).length) {
      return value;
    }
    return value.filter((item) => {
      return paramsName.every((key) => {
        return item[key].includes(args[key]);
      });
    });
  }
}
