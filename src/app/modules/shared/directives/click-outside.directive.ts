import {EventEmitter, HostListener, Output} from '@angular/core';
import {Directive, ElementRef} from '@angular/core';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[clickOutside]',
})
export class ClickOutsideDirective {
  @Output() clickOutside = new EventEmitter<void>();

  constructor(private elementRef: ElementRef) {}

  @HostListener('document:click', ['$event'])
  // tslint:disable-next-line: typedef
  public documentClick(event: MouseEvent) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    const clickedInside = this.elementRef.nativeElement.contains(event.target);
    if (!clickedInside) {
      this.clickOutside.emit();
    }
  }
}
