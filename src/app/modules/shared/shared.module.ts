import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {ClickOutsideDirective} from './directives/click-outside.directive';
import {FilterPipe} from './pipes/filter.pipe';

const EXPORTS = [ClickOutsideDirective, FilterPipe];

@NgModule({
  declarations: [EXPORTS],
  imports: [CommonModule, HttpClientModule],
  exports: [EXPORTS],
})
export class SharedModule {}
