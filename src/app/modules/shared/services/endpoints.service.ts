import {Injectable} from '@angular/core';
import {environment} from '@environment';

@Injectable({
  providedIn: 'root',
})
export class EndpointsService {
  private baseApiUrl = environment.baseApiUrl;
  public peoples = `${this.baseApiUrl}/people`;
  public films = `${this.baseApiUrl}/films`;
  public species = `${this.baseApiUrl}/species`;
  constructor() {}
}
