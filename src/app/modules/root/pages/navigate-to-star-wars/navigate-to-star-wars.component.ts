import {Component} from '@angular/core';
import {AppRoutes} from '@modules/root/app.routes';

@Component({
  selector: 'sw-navigate-to-star-wars',
  templateUrl: './navigate-to-star-wars.component.html',
  styleUrls: ['./navigate-to-star-wars.component.scss'],
})
export class NavigateToStarWarsComponent {
  public appRoutes = AppRoutes;
  constructor() {}
}
