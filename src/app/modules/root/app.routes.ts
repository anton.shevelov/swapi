import {CharactersRoutes} from '@modules/characters/characters.routes';
import {InAppRoutes} from '@modules/shared/classes/in-app.routes';

export class AppRoutes extends InAppRoutes {
  public static ROOT = '';
  public static WELCOME = 'welcome';
  public static CHARACTERS = CharactersRoutes.CHARACTERS_ROOT;
}
