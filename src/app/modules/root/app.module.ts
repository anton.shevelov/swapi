import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {CommonModule} from '@angular/common';
import {AppComponent} from './pages/app/app.component';
import {NavigateToStarWarsComponent} from './pages/navigate-to-star-wars/navigate-to-star-wars.component';
import {CharactersModule} from '@modules/characters/characters.module';
import {PageNotFoundComponent} from '../root/pages/page-not-found/page-not-found.component';
import {SharedModule} from '@modules/shared/shared.module';

@NgModule({
  declarations: [AppComponent, NavigateToStarWarsComponent, PageNotFoundComponent],
  imports: [CommonModule, BrowserModule, AppRoutingModule, CharactersModule, SharedModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
