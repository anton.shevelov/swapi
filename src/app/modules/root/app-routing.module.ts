import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NavigateToStarWarsComponent} from './pages/navigate-to-star-wars/navigate-to-star-wars.component';
import {AppRoutes} from './app.routes';
import {PageNotFoundComponent} from '@modules/root/pages/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: AppRoutes.ROOT,
    redirectTo: AppRoutes.WELCOME,
    pathMatch: 'full'
  },
  {
    path: AppRoutes.WELCOME,
    component: NavigateToStarWarsComponent,
  },
  {
    path: AppRoutes.ROOT,
    children: [
      {
        path: AppRoutes.CHARACTERS,
        loadChildren: () =>
          import('../characters/characters.module').then((module) => module.CharactersModule),
      },
    ],
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
